# ZFile-美化版

#### 简介
网站:[www.ucu520.top](http://www.ucu520.top)

效果请看:[http://zfile.ucu520.top](http://zfile.ucu520.top)

修改自[https://github.com/zhaojun1998/zfile](https://github.com/zhaojun1998/zfile)

#### 下载地址
[https://gitee.com/ucu520/zfile-beautified-version/releases](https://gitee.com/ucu520/zfile-beautified-version/releases)

> 需要什么版本的，自行下载
这里的版本号不是迭代更新，而是不同版本之间为了区别才加的
> 别下载 Source code (zip)，Source code (tar.gz)
源码里面不包含文件，这里没法批量上传，源码就不在这上传了，自行下载程序并解压即可

#### 修改说明
我个人感觉原版的背景不好看
自己修改了一部分背景
可能有部分人有需求用到这个的源文件
因此公布出来

#### 安装教程

和[ZFile](https://github.com/zhaojun1998/zfile)本身的一样
文档地址: [http://docs.zhaojun.im/zfile](http://docs.zhaojun.im/zfile)



#### 安装依赖环境:
(来源GitHub的源介绍)

```bash
# CentOS系统
yum install -y java-1.8.0-openjdk unzip
```

```bash
# Debian 9 / Ubuntu 14+
apt update
apt install -y openjdk-8-jre-headless unzip
```

```bash
# Debian 10 (Buster) 系统
apt update && apt install -y apt-transport-https software-properties-common ca-certificates dirmngr gnupg
wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add -
add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
apt update && apt install -y adoptopenjdk-8-hotspot-jre
```


> 如为更新程序, 则请先执行 `~/zfile/bin/stop.sh && rm -rf ~/zfile` 清理旧程序. 首次安装请忽略此选项.

> Windows环境下，先安装JAVA，JDK8，并配置环境变量

#### 程序的目录结构为:
```
├── zfile
    ├── META-INF
    ├── WEB-INF
    └── bin
        ├── start.sh    # 启动脚本
        └── stop.sh     # 停止脚本
        ├── restart.sh  # 重启脚本
```

#### 我修改的目录结构为:
我主要改动了前端部分
```
├── zfile
    ├── META-INF
    ├── WEB-INF
        ├── classee
            ├── application.yml # Linux中，这里修改端口号及路劲等
            ├── static  # 前端部分
        ├── lib
    └── bin
```

#### 程序运行

> 修改yml文件
```
[建议]
修改application.yml文件[我测试时候，修改过路劲，端口号等信息]
建议根据个人情况进行修改下

zfile:
  debug: false
  directLinkPrefix: directlink
  log:
    path: ${user.home}/.zfile/logs        # 存放日志文件的路劲
  db:
    path: ${user.home}/.zfile/db/zfile    #存放数据库文件的路劲
  tmp:
    path: ${user.home}/.zfile/tmp         #缓存文件的存放路劲
  cache:
    auto-refresh:
      interval: 1
    timeout: 1800                        # 缓存刷新时间
  constant:
    readme: readme.md                    # 如果打开的目录下有这个文件，会默认显示内容
    password: password.txt               # 如果需要给一个目录设置密码，将密码保存到这个文件中即可

server:
  port: 8080    # 这里是服务运行的端口号

```

> Linux下运行[刚刚进入到解压后的文件夹开始]
```
./bin/start.sh

# 以下为完整命令
cd /bin
chmod -R 777 *
./start.sh   # 这句可以改成 bash ./start.sh
```

浏览器打开[ip]:[端口号]
Eg:127.0.0.1:8080

> Windows下运行
```
# 打开命令提示符，进入ZFile到文件夹中
java -Dfile.encoding=utf-8 -jar -Dserver.port=8088 .\zfile-release.jar
# -Dserver.port=8088    这里的8088是端口号，根据需求自行修改
# .\zfile-release.jar    这是下载后的文件，不需要解压
```
浏览器打开[ip]:[端口号]
Eg:127.0.0.1:8088