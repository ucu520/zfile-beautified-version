
#### 安装依赖环境:
(来源GitHub的源介绍)

```bash
# CentOS系统
yum install -y java-1.8.0-openjdk unzip
```

```bash
# Debian 9 / Ubuntu 14+
apt update
apt install -y openjdk-8-jre-headless unzip
```

```bash
# Debian 10 (Buster) 系统
apt update && apt install -y apt-transport-https software-properties-common ca-certificates dirmngr gnupg
wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add -
add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
apt update && apt install -y adoptopenjdk-8-hotspot-jre
```

### 运行
> 到bin目录下
> 
> ./restart.sh

### 公布初衷
> 最近大量精力都用到了Alist上，服务器配置本不高，加上每天都有攻击，如果套CF，那么用户访问速度会受到很大的局限性，因此Alist和Zfile这两个下载站，到目前为止都是直接跑服务器的ip，没做任何的隐藏。

> 阿里云服务器也快过期了，2023年02月过期。过期后，数据量将全部转到仅有的腾讯云的轻量上。加上目前已有的网站等来看，轻量应用服务器的压力会变的很大很大，因此打算关闭zfile站点，或许说，关闭zfile3的站点，用4的站点，又或者zfile的站点都关了，全部转向Alist的站点。

> 介于多方面考虑，因此将本站目前用的Zfile代码全部开源，感兴趣的可以跑了看看，也可以把导航页等信息放其他地方跑

### 重心Alist简介
> Zfile的耗费资源（目前的这个3），资源消耗比较大，目前一个服务器挂着一个Zfile，阿里的挂着3，也就是开源这个，腾讯云的挂着4，不是最新的，没做什么改变，都是官方直接拿来用的

> 对我而言，Zfile的webdav起始点有点高

> 存储源来看，目前我比较偏向Alist

> Ps:只是重心偏向Alist，不代表我不用Zfile。各有各的好

### Alist站点地址
> [alist.ucu520.top](https://alist.ucu520.top)

### 友情提示
> 本项目内的所有文件为网站[zfile.ucu520.top](https://zfile.ucu520.top)拿到的数据
> 由于我个人改了部分东西，如需使用，请自行修改下文件等参数
> 
> 配置文件地址：
> ./WEB-INF/classes/application.yml
> 
> 项目端口号为：55678
> 其余的信息请自行修改 


### 4-9行路劲简介
> 
> /www/wwwroot/zfile.ucu520.top/zfile/ --- 这个目录为我网站所在目录，请自行替换为你自己的。也就是本项目的所有文件所在路劲
> 
> 

### 前端部分简介
> 我主要更改了前端部分，位置
> 
> ./WEB-INF/classes/static

> 这里的内容都是前端的内容，文件不做过多整理了，你可以看着改，如果需要看板娘的话，前端这个目录下能找到我修改时留下的东西
> 
> 其余的，可自行处理
> 
> Ps:index部分，布局来自Netflix

### 部分改动简介
> ./WEB-INF/lib
> 
> 全部替换过比3.2新的版本，具体版本号忘了

> ./WEB-INF/classes/static
> 
> 也有替换

> 启动脚本也改过
