#### 需要修改的话，请下载
https://gitee.com/ucu520/zfile-beautified-version/releases
#### 解压后，根据需求修改即可
```
├── zfile
    ├── META-INF
    ├── WEB-INF
        ├── classee
            ├── application.yml # Linux中，这里修改端口号及路劲等
            ├── static  # 前端部分
        ├── lib
    └── bin
```

#### 修改后添加至Windows使用的jar，下载

这个是3.1版本的，2021-09-27之前发布的都是3.1版本的
https://gitee.com/ucu520/zfile-beautified-version/attach_files/830538/download/zfile_%E5%B9%BB%E7%81%AF%E7%89%87%E8%83%8C%E6%99%AF-Windows.jar
> 使用压缩软件打开，不要解压，直接使用压缩软件打开，RAR，等软件都行
> BOOT-INF\classes对应的路劲就是WEB-INF
自行替换里面的文件，运行一样
java -Dfile.encoding=utf-8 -jar -Dserver.port=44668 zfile_幻灯片背景-Windows.jar
