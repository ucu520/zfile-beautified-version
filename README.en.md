# ZFile-Beauty Edition

#### Introduction
Website: [www.ucu520.top](http://www.ucu520.top)

See the effect: [http://zfile.ucu520.top](http://zfile.ucu520.top)

Modified from [https://github.com/zhaojun1998/zfile](https://github.com/zhaojun1998/zfile)

#### download link
[https://gitee.com/ucu520/zfile-beautified-version/releases](https://gitee.com/ucu520/zfile-beautified-version/releases)

> What version is needed, download by yourself
The version number here is not an iterative update, but is added for distinction between different versions
> Don’t download Source code (zip), Source code (tar.gz)
The source code does not contain files, and there is no way to upload them in batches. The source code will not be uploaded here, just download the program and unzip it by yourself.

#### Modify the description
I personally feel that the background of the original version is not good
I modified part of the background myself
There may be some people who need to use this source file
So announced

#### Installation tutorial

Same as [ZFile](https://github.com/zhaojun1998/zfile) itself
Document address: [http://docs.zhaojun.im/zfile](http://docs.zhaojun.im/zfile)



#### Installation dependent environment:
(Source Introduction to GitHub)

```bash
# CentOS system
yum install -y java-1.8.0-openjdk unzip
```

```bash
# Debian 9 / Ubuntu 14+
apt update
apt install -y openjdk-8-jre-headless unzip
```

```bash
# Debian 10 (Buster) system
apt update && apt install -y apt-transport-https software-properties-common ca-certificates dirmngr gnupg
wget -qO-https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add-
add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
apt update && apt install -y adoptopenjdk-8-hotspot-jre
```


> If it is an update program, please execute `~/zfile/bin/stop.sh && rm -rf ~/zfile` to clean up the old program. Please ignore this option for the first installation.

> In Windows environment, first install JAVA, JDK8, and configure environment variables

#### The directory structure of the program is:
```
├── zfile
    ├── META-INF
    ├── WEB-INF
    └── bin
        ├── start.sh # start script
        └── stop.sh # Stop the script
        ├── restart.sh # restart script
```

#### The directory structure I modified is:
I mainly changed the front-end part
```
├── zfile
    ├── META-INF
    ├── WEB-INF
        ├── classee
            ├── application.yml # In Linux, modify the port number and road King, etc.
            ├── static # front-end part
        ├── lib
    └── bin
```

#### Program running

> Modify the yml file
```
[Suggest]
Modify the application.yml file [When I tested, modify the information such as the road force, port number, etc.]
It is recommended to modify according to personal circumstances

zfile:
  debug: false
  directLinkPrefix: directlink
  log:
    path: ${user.home}/.zfile/logs # Path to store log files
  db:
    path: ${user.home}/.zfile/db/zfile #The way to store database files
  tmp:
    path: ${user.home}/.zfile/tmp #Storage path of cache files
  cache:
    auto-refresh:
      interval: 1
    timeout: 1800 # Cache refresh time
  constant:
    readme: readme.md # If there is this file in the opened directory, the content will be displayed by default
    password: password.txt # If you need to set a password for a directory, save the password to this file

server:
  port: 8080 # Here is the port number where the service is running

```

> Run under Linux [Just enter the unzipped folder and start]
```
./bin/start.sh

# The following is the complete command
cd /bin
chmod -R 777 *
./start.sh # This sentence can be changed to bash ./start.sh
```

Open the browser [ip]:[port number]
Eg:127.0.0.1:8080

> Run under Windows
```
# Open the command prompt, enter ZFile to the folder
java -Dfile.encoding=utf-8 -jar -Dserver.port=8088 .\zfile-release.jar
# -Dserver.port=8088 where 8088 is the port number, modify it according to your needs
# .\zfile-release.jar This is the downloaded file, no need to unzip
```
Open the browser [ip]:[port number]
Eg:127.0.0.1:8088